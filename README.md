# gtkslides

## To build and install

You need the [stack](https://docs.haskellstack.org) build tool and gtk
installed.

To build:

Enter the project root directory (`gtkslides`). Then run:

    stack setup
    stack install gtk2hs-buildtools
    stack install

The second step might not be necessary.


## Usage

Pipe files to the application and it will show thumbnails of images.

    find ~/Pictures -type f | viewthumbs
