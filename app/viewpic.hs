{-# LANGUAGE OverloadedStrings #-}

module Main where

import Control.Monad.IO.Class (liftIO)
import Data.Bifunctor (bimap)
import Graphics.UI.Gtk as Gtk
import System.Exit (die)

import qualified Graphics.Rendering.Cairo as Cairo

import Settings
import Utils

--------------------------------------------------------------------------------

maxWinWidth = 1024
maxWinHeight = 768

main = do
   args <- Gtk.initGUI

   file <- do
      case args of
         [arg1] -> return arg1
         _ -> die "USAGE: [file]"

   pixbuf <- pixbufNewFromFile file
   Size imgWidth imgHeight <- pixbufGetSize pixbuf
   let imgR = fromIntegral imgWidth / fromIntegral imgHeight

   win <- windowNew
   set win [ windowTitle := file ]

   on win deleteEvent (liftIO Gtk.mainQuit >> return True)

   on win keyPressEvent $ tryEvent $ do
      "Escape" <- eventKeyName
      liftIO mainQuit

   -- Extend window to fit image
   on win keyPressEvent $ tryEvent $ do
      "e" <- eventKeyName
      liftIO $ do
         (winW, winH) <- bimap fromIntegral fromIntegral <$> windowGetSize win
         let (w,h) = if imgR < winW / winH then (winW, winW / imgR) else (imgR * winH, winH)
         windowResize win (round w) (round h)

   -- Shrink window to fit image
   on win keyPressEvent $ tryEvent $ do
      "f" <- eventKeyName
      liftIO $ do
         (winW, winH) <- bimap fromIntegral fromIntegral <$> windowGetSize win
         let (w,h) = if imgR < winW / winH then (imgR * winH, winH) else (winW, winW / imgR)
         windowResize win (round w) (round h)

   da <- drawingAreaNew

   widgetModifyBg win StateNormal backgroundColor

   on da draw $ do
      renderImage da pixbuf

   containerAdd win da

   windowSetDefaultSize win (min imgWidth maxWinWidth) (min imgHeight maxWinHeight)

   widgetShowAll win

   mainGUI


renderImage :: DrawingArea -> Pixbuf -> Cairo.Render ()
renderImage dest image = do
   destSize <- liftIO $ widgetGetAllocatedSize dest
   imageSize <- liftIO $ pixbufGetSize image

   let destRect = Rect (Pos 0 0) (fromIntegral <$> destSize)
   let Rect (Pos x y) (Size w h) = fitInRect destRect (fromIntegral <$> imageSize)

   scaled <- liftIO (pixbufScaleSimple image (round w) (round h) InterpBilinear)

   setSourcePixbuf scaled x y
   Cairo.paint
