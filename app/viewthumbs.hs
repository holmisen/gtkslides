{-# LANGUAGE ParallelListComp, NamedFieldPuns, OverloadedStrings, NoMonomorphismRestriction #-}

module Main where

import Data.Maybe
import Data.IORef
import Control.Monad (guard)
import Control.Monad.IO.Class (liftIO)
import Graphics.UI.Gtk as Gtk hiding (Size)
import System.IO (hPutStrLn, stderr)

import qualified Data.List as List
import qualified Graphics.Rendering.Cairo as Cairo
import qualified System.Process as Proc
import qualified Streaming.Prelude as S

import Data.Vector (Vector, (!))
import qualified Data.Vector as V

import ThumbsProvider (Thumb(..), ThumbsProvider, thumbPixbuf, thumbFile, thumbSize)
import qualified ThumbsProvider

import Settings (backgroundColor)
import Utils

--------------------------------------------------------------------------------

cellMargin :: Num a => a
cellMargin = 15

cellAlloc :: Num a => a
cellAlloc = thumbSize + cellMargin

imageViewer = "viewpic"

--------------------------------------------------------------------------------

main :: IO ()
main = do
   thumbs <- ThumbsProvider.new S.stdinLn

   pageOffset <- newIORef 0
   page <- newIORef []

   Gtk.initGUI

   win <- windowNew
   set win [ windowTitle := ("viewthumbs"::String) ]

   windowSetDefaultSize win 1000 800

   on win deleteEvent (liftIO Gtk.mainQuit >> return True)

   da <- drawingAreaNew

   widgetModifyBg win StateNormal backgroundColor

   let pgUp   = liftIO $ scrollUp da pageOffset
   let pgDown = liftIO $ scrollDown da thumbs pageOffset

   on win keyPressEvent $ tryEvent $ do
      key <- eventKeyName
      guard (key `elem` ["space", "Down", "Page_Down"])
      pgDown

   on win keyPressEvent $ tryEvent $ do
      key <- eventKeyName
      guard (key `elem` ["BackSpace", "Up", "Page_Up"])
      pgUp

   on win keyPressEvent $ tryEvent $ do
      "Escape" <- eventKeyName
      liftIO mainQuit

   on win keyPressEvent $ do
      key <- eventKeyName
      liftIO (print key)
      return False

   on da scrollEvent $ tryEvent $ do
      ScrollDown <- eventScrollDirection
      pgDown

   on da scrollEvent $ tryEvent $ do
      ScrollUp <- eventScrollDirection
      pgUp

   on da buttonPressEvent $ do
      (x,y) <- eventCoordinates
      liftIO $ do
         clickThumb (truncate x, truncate y) =<< readIORef page
      return True

   on da draw $ do
      pg <- liftIO $ do
         po <- liftIO $ readIORef pageOffset
         pg <- liftIO $ createPage da thumbs po
         writeIORef page pg
         return pg
      renderPage pg

   containerAdd win da

   widgetShowAll win

   mainGUI

--------------------------------------------------------------------------------

scrollDown :: DrawingArea -> ThumbsProvider -> IORef Int -> IO ()
scrollDown da thumbs refPageOffset = do
   -- calculate new offset
   i <- readIORef refPageOffset
   n <- getPageSize <$> widgetGetAllocatedSize da
   xs <- ThumbsProvider.getSlice (i+n) n thumbs
   let i' = i + min n (length xs)

   writeIORef refPageOffset i'
   widgetQueueDraw da


scrollUp :: DrawingArea -> IORef Int -> IO ()
scrollUp da refPageOffset = do
   n <- getPageSize <$> widgetGetAllocatedSize da
   modifyIORef refPageOffset (\x -> max 0 (x - n))
   widgetQueueDraw da


clickThumb :: (Int,Int) -> Page -> IO ()
clickThumb (x,y) page = do
   let p = Pos (fromIntegral x) (fromIntegral y)
   case List.find (\(_, r) -> rectContainsPos p r) page of
      Nothing ->
         return ()
      Just (t, _) -> do
         Proc.createProcess $
            (Proc.proc imageViewer [thumbFile t]) { Proc.std_in = Proc.NoStream }
         return ()

--------------------------------------------------------------------------------

data PageLayout = PageLayout { columns, rows, columnWidth, rowHeight :: Int }
   deriving (Eq, Show)


getPageSize :: Size Int -> Int
getPageSize size =
   let PageLayout { columns, rows } = getPageLayout size
   in (columns * rows)


getPageLayout :: Size Int -> PageLayout
getPageLayout (Size width height) =
   let
      cols = max 1 (width `div` cellAlloc)
      cw = width `div` cols

      rows = max 1 (height `div` cellAlloc)
      rh = height `div` rows
   in
      PageLayout cols rows cw rh

--------------------------------------------------------------------------------

type Page = [(Thumb, Rect Double)]

createPage :: DrawingArea -> ThumbsProvider -> Int -> IO Page
createPage dest thumbsProvider offset = do
   PageLayout { columns
              , rows
              , columnWidth
              , rowHeight } <- getPageLayout <$> liftIO (widgetGetAllocatedSize dest)

   let cols = columns
   let cw = columnWidth
   let rh = rowHeight

   let create (thumb, cellRect) = do
          let pic = thumbPixbuf thumb
          picSize <- pixbufGetSize pic
          let thumbRect = centerIn (fromIntegral <$> cellRect) (fromIntegral <$> picSize)
          return (thumb, thumbRect)

   thumbs <- liftIO (ThumbsProvider.getSlice offset (cols * rows) thumbsProvider)

   mapM create
      [ (t, Rect (Pos (c * cw) (r * rh)) (Size cellAlloc cellAlloc))
      | r <- [0 .. rows - 1]
      , c <- [0 .. cols - 1]
      | t <- thumbs
      ]


renderPage :: Page -> Cairo.Render ()
renderPage thumbs = mapM_ renderThumb thumbs

renderThumb :: (Thumb, Rect Double) -> Cairo.Render ()
renderThumb (thumb, Rect (Pos x y) _) = do
   setSourcePixbuf (thumbPixbuf thumb) x y
   Cairo.paint
