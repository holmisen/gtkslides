{-# LANGUAGE NamedFieldPuns #-}

module Provider
  ( Provider
  , new
  , getSlice
  , getLoaded
  )
where

import Control.Concurrent (forkIO)
import Control.Concurrent.BoundedChan
import Data.Maybe
import Data.Monoid
import Data.IORef
import Data.Vector (Vector)
import Streaming (Stream, Of(..))
import qualified Data.Vector as V
import qualified Streaming.Prelude as S

--------------------------------------------------------------------------------

data Provider a = Provider
   { loaded :: IORef (Vector a)
   , provided :: IORef [a]
   }


new :: Stream (Of a) IO () -> IO (Provider a)
new s =
   Provider <$> newIORef V.empty <*> (newIORef =<< generate s)


getSlice :: Int -> Int -> Provider a -> IO [a]
getSlice offset size Provider { loaded, provided } = do
   -- load needed elements
   nl <- V.length <$> readIORef loaded
   (xs2, s) <- splitAt (offset + size - nl) <$> readIORef provided
   modifyIORef loaded (<> V.fromList xs2)
   writeIORef provided s

   -- get slice
   xs1 <- (V.take size . V.drop offset) <$> readIORef loaded
   return (V.toList xs1)


getLoaded :: Provider a -> IO (Vector a)
getLoaded = readIORef . loaded

--------------------------------------------------------------------------------

generate :: Stream (Of a) IO () -> IO [a]
generate stream = do
   ch <- newBoundedChan 100

   let loop s = do
          m <- S.uncons s
          case m of
             Nothing ->
                writeChan ch Nothing
             Just (a, s') -> do
                writeChan ch (Just a)
                loop s'

   forkIO (loop stream)
   (catMaybes . takeWhile isJust) <$> getChanContents ch
