module ThumbsProvider
  ( ThumbsProvider
  , new
  , getSlice
  , getLoaded
  
  , Thumb(..)
  , thumbSize
  , loadThumb
  , maybeLoadThumb
  )
where

import Data.Vector (Vector)
import Graphics.UI.Gtk as Gtk
import System.Glib.GError (catchGErrorJustDomain, catchGError)
import System.IO (hPutStrLn, stderr)

import Streaming (Stream, Of)
import qualified Streaming.Prelude as S

import Provider (Provider)
import qualified Provider

--------------------------------------------------------------------------------

thumbSize :: Num a => a
thumbSize = 150

--------------------------------------------------------------------------------

data Thumb = Thumb { thumbFile :: FilePath, thumbPixbuf :: Pixbuf }


loadThumb :: FilePath -> IO Thumb
loadThumb file =
   Thumb file <$> pixbufNewFromFileAtSize file thumbSize thumbSize


maybeLoadThumb :: FilePath -> IO (Maybe Thumb)
maybeLoadThumb file =
   catchGError (Just <$> loadThumb file) $
      \err -> do
         hPutStrLn stderr (show err)
         return Nothing

--------------------------------------------------------------------------------

data ThumbsProvider = ThumbsProvider (Provider Thumb)


new :: Stream (Of FilePath) IO () -> IO ThumbsProvider
new files =
   ThumbsProvider <$> (Provider.new $ S.catMaybes $ S.mapM maybeLoadThumb $ files)


getSlice :: Int -> Int -> ThumbsProvider -> IO [Thumb]
getSlice offset size (ThumbsProvider p) = Provider.getSlice offset size p


getLoaded :: ThumbsProvider -> IO (Vector Thumb)
getLoaded (ThumbsProvider p) = Provider.getLoaded p
