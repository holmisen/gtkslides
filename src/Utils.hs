{-# LANGUAGE DeriveFunctor #-}

module Utils where

import Graphics.UI.Gtk (
   widgetGetAllocatedHeight,
   widgetGetAllocatedWidth,
   pixbufGetHeight,
   pixbufGetWidth,
   Pixbuf,
   WidgetClass,
   Color )

--------------------------------------------------------------------------------

data Size a = Size { width, height :: !a }
   deriving (Eq, Ord, Show, Functor)

data Pos a = Pos { posX, posY :: !a }
   deriving (Eq, Ord, Show, Functor)

data Rect a = Rect { rectPos :: Pos a, rectSize :: Size a }
   deriving (Eq, Ord, Show, Functor)

--------------------------------------------------------------------------------

scaleSizeTo :: (Ord a, Fractional a) => Size a -> Size a -> Size a
scaleSizeTo (Size w1 h1) (Size w h) = Size (s * w) (s * h)
   where s = min (h1 / h) (w1 / w)

scaleRectTo :: (Ord a, Fractional a) => Size a -> Rect a -> Rect a
scaleRectTo s1 (Rect p s) = Rect p (scaleSizeTo s1 s)

centerIn :: (Ord a, Fractional a) => Rect a -> Size a -> Rect a
centerIn (Rect (Pos x1 y1) (Size w1 h1)) s@(Size w h) = Rect (Pos x y) s
   where
      x = x1 + (w1 - w) / 2
      y = y1 + (h1 - h) / 2

fitInRect :: (Ord a, Fractional a) => Rect a -> Size a -> Rect a
fitInRect r1 s = centerIn r1 $ scaleSizeTo (rectSize r1) s

rectContainsPos :: (Ord a, Num a) => Pos a -> Rect a -> Bool
rectContainsPos (Pos x y) (Rect (Pos x1 y1) (Size w h)) =
   x1 <= x && x <= x1 + w &&
   y1 <= y && y <= y1 + h

--------------------------------------------------------------------------------

widgetGetAllocatedSize :: WidgetClass self => self -> IO (Size Int)
widgetGetAllocatedSize w =
   Size <$> widgetGetAllocatedWidth w <*> widgetGetAllocatedHeight w


pixbufGetSize :: Pixbuf -> IO (Size Int)
pixbufGetSize b =
   Size <$> pixbufGetWidth b <*> pixbufGetHeight b
